package ar.com.alfersoft.scoreboard;

import java.util.LinkedList;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

public class GameScore extends Activity {

	/* Message types sent from the BluetoothChatService Handler */
	public static final int MESSAGE_STATE_CHANGE = 1;
	
	/* service codes */
	public static final int SERVICE_A_UP	= 1;
	public static final int SERVICE_B_UP	= 2;
	public static final int SERVICE_A_DOWN	= 4;
	public static final int SERVICE_B_DOWN	= 8;

	/* locals */
	private int						startingService=SERVICE_A_UP;
	private int						currentService,offsetService;
	private LinkedList<undoInfo>	stackList = new LinkedList<undoInfo>();
	private int						teamAScore,teamBScore;
	private RadioButton				Aup,Adn,Bup,Bdn,LastRadio,connectStatus,mode11;
	private EditText				Ascore,Bscore;
	private ImageButton				addA,addB,restartGame,undoOperation;
	private TextView				statusBar; 
	private ScoreBoardService		service;
	private	final int				REQUEST_CONNECT = 0x674656;
	private BluetoothAdapter		adp = BluetoothAdapter.getDefaultAdapter();
	private PowerManager.WakeLock	wl;
	private long					keyDownAt;
	
	@Override
	protected void onCreate(Bundle bundle) {
		// TODO Auto-generated method stub
		super.onCreate(bundle);
		setContentView(R.layout.score);

		/* radio buttons listener */
		CompoundButton.OnCheckedChangeListener serviceChecked = new CompoundButton.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(LastRadio != null && buttonView.getId() != LastRadio.getId()) {
					LastRadio.setChecked(false);
				}
				LastRadio=(RadioButton) buttonView;
			}
		};
		
		/* verify initial position of the current Service */
		
		View.OnClickListener checkService = new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				switch(v.getId()) {
				case R.id.serviceAUp:
					currentService = startingService = SERVICE_A_UP;
					break;
				case R.id.serviceBUp:
					currentService = startingService = SERVICE_B_UP;
					break;
				case R.id.serviceADown:
					currentService = startingService = SERVICE_A_DOWN;
					break;
				case R.id.serviceBDown:
					currentService = startingService = SERVICE_B_DOWN;
					break;
				}
				showService();
			}
		};
		
		/* edit text scores listener */
		View.OnClickListener addPoint = new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				addScorePoint(v.getId() == R.id.addPointA ? 0 : 1);
			}
				
		};
		
		/* get services radio button */
		Aup = (RadioButton) findViewById(R.id.serviceAUp);
		Adn = (RadioButton) findViewById(R.id.serviceADown);
		Bup = (RadioButton) findViewById(R.id.serviceBUp);
		Bdn = (RadioButton) findViewById(R.id.serviceBDown);
		
		/* get edit texts */
		
		Ascore = (EditText) findViewById(R.id.teamA);
		Bscore = (EditText) findViewById(R.id.teamB);

		/* set check listener */
		Aup.setOnCheckedChangeListener(serviceChecked);
		Adn.setOnCheckedChangeListener(serviceChecked);
		Bup.setOnCheckedChangeListener(serviceChecked);
		Bdn.setOnCheckedChangeListener(serviceChecked);
		
		/* set click listener */
		Aup.setOnClickListener(checkService);
		Adn.setOnClickListener(checkService);
		Bup.setOnClickListener(checkService);
		Bdn.setOnClickListener(checkService);
		
		/* compute points */
		
		addA = (ImageButton) findViewById(R.id.addPointA);
		addB = (ImageButton) findViewById(R.id.addPointB);
		
		/* set listener */
		
		addA.setOnClickListener(addPoint);
		addB.setOnClickListener(addPoint);
		
		/* restart game */
		
		restartGame = (ImageButton) findViewById(R.id.restartGame);
		restartGame.setOnLongClickListener(new OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View v) {
				if (hasMatchEnded()) {
					nextGame();
				} else {
					resetGame();
				}
				AudioManager mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
				mAudioManager.playSoundEffect(AudioManager.FX_KEYPRESS_RETURN,10);
				return false;
			}
		});
		
		restartGame.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
			}
		});
		
		/* undo operation */
		
		undoOperation = (ImageButton) findViewById(R.id.undoOperation);
		undoOperation.setOnLongClickListener(new OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View v) {
				undo();
				return false;
			}
		});
		
		/* status bar */		
		statusBar = (TextView) findViewById(R.id.statusBar);
		
		/* bluetooth service */
		service = new ScoreBoardService(this, mHandler);		

		/* connect status */		
		connectStatus = (RadioButton) findViewById(R.id.connectStatus);
		connectStatus.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				if (adp.isEnabled()) {
					if(service.getState() == ScoreBoardService.STATE_CONNECTED) {
						service.stop();
					} else {
						Intent intent = new Intent(GameScore.this,ScoreBoardDevices.class);
						startActivityForResult(intent,REQUEST_CONNECT);
					}
				} else {
					setStatus("Bluetooth is not active");
				}
			}
		});
		connectStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked && service.getState() != ScoreBoardService.STATE_CONNECTED) ((RadioButton) buttonView).setChecked(false);
			}
		});
		
		/* game mode */
		
		mode11 = (RadioButton) findViewById(R.id.mode11);
		//mode21 = (RadioButton) findViewById(R.id.mode21);
		
		/* settings to avoid sleeping */
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "scoreboardtag");
	}

	/* use volume buttons to increase score */
	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		int action = event.getAction();
		int keyCode = event.getKeyCode();

		if (keyCode != KeyEvent.KEYCODE_VOLUME_UP && keyCode != KeyEvent.KEYCODE_VOLUME_DOWN) {
			return super.dispatchKeyEvent(event);
		}
		
		long elapsed = 0;
		if (action == KeyEvent.ACTION_UP) {
			elapsed = (System.currentTimeMillis() - keyDownAt);
			keyDownAt = 0;
		} else if (action == KeyEvent.ACTION_DOWN && keyDownAt == 0) {
			keyDownAt = System.currentTimeMillis();
		}
		if (action != KeyEvent.ACTION_UP) {
			return true;
		}
		switch (keyCode) {
		case KeyEvent.KEYCODE_VOLUME_UP:
			if (elapsed >= 2000 && hasMatchEnded()) {
				nextGame();
			} else if (elapsed >= 500) {
				undo();
			} else {
				addScorePoint(0);
			}
			return true;
		case KeyEvent.KEYCODE_VOLUME_DOWN:
			if (elapsed >= 2000 && hasMatchEnded()) {
				nextGame();
			} else if (elapsed >= 500) {
				undo();
			} else {
				addScorePoint(1);
			}
			return true;
		default:
			return super.dispatchKeyEvent(event);
		}
	}	
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		service.stop();
		/* allow sleeping again */
		wl.release();
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		/* reset initial game */
		resetGame();
		/* avoid sleeping (this will disconnect bluetooth */
		wl.acquire();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if(resultCode != RESULT_OK) return;
		switch(requestCode) {
		case REQUEST_CONNECT:
			
			if (adp.isEnabled()) {
				// Get the device MAC address
				String address = data.getExtras().getString(ScoreBoardDevices.EXTRA_DEVICE_ADDRESS);
				// Get the BluetoothDevice object
				BluetoothDevice device = adp.getRemoteDevice(address);
				// Attempt to connect to the device
				service.connect(device);
			} else {
				setStatus("Bluetooth is not active");
			}
			break;
		}
	}	
	
	private void undo()
	{
		if(stackList.size() > 0) {
			undoInfo u = stackList.removeLast();
			currentService = u.service;
			teamAScore = u.Ascore;
			teamBScore = u.Bscore;
			showScores();
			showService();
			setStatus(String.format("Undo: A=%d B=%d",u.Ascore,u.Bscore));
		} else {
			resetGame();
		}
		AudioManager mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
		mAudioManager.playSoundEffect(AudioManager.FX_KEYPRESS_DELETE,10);
	}
	
	/* reset to a new game */
	private void resetGame()
	{
		setStatus("");
		startingService = SERVICE_A_UP;
		currentService = startingService;
		stackList.clear();
		teamAScore = 0;
		teamBScore = 0;
		enableService();
		showService();
		showScores();
	}

	/* reset to a new game */
	private void nextGame()
	{
		setStatus("Next game!");
		switch (startingService) {
		case SERVICE_A_UP:
			startingService = SERVICE_B_DOWN;
			break;
		case SERVICE_B_UP:
			startingService = SERVICE_A_UP;
			break;
		case SERVICE_A_DOWN:
			startingService = SERVICE_B_UP;
			break;
		case SERVICE_B_DOWN:
			startingService = SERVICE_A_DOWN;
			break;
		default: /* should not happen... */
			startingService = SERVICE_A_UP;
			break;
		}
		currentService = startingService;
		stackList.clear();
		teamAScore = 0;
		teamBScore = 0;
		enableService();
		showService();
		showScores();
		AudioManager mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
		mAudioManager.playSoundEffect(AudioManager.FX_KEYPRESS_RETURN,10);
	}
	
	/* check if the match has ended */
	private boolean hasMatchEnded() {
		/* max number of points for this match */
		final int max = (mode11.isChecked() ? 11 : 21);
		
		/* verify if match has ended */
		if(Math.max(teamAScore,teamBScore) >= max && Math.max(teamAScore,teamBScore)-Math.min(teamAScore,teamBScore) >= 2) {
			return true;
		}
		return false;
	}
	
	/* add a point on the scored */
	public void addScorePoint(int team) {
		/* verify if match has ended */
		if(hasMatchEnded()) {
			showEnd(teamAScore,teamBScore);
			return;
		}
		
		/* verify if we are starting from a different offset service */

		if(stackList.size() == 0) {
			currentService = startingService;
			switch(currentService) {
			case SERVICE_A_UP:
				offsetService = 0;
				break;
			case SERVICE_B_UP:
				offsetService = 6;
				break;
			case SERVICE_A_DOWN:
				offsetService = 4;
				break;
			case SERVICE_B_DOWN:
				offsetService = 2;
				break;
			}
			disableService();
		}


		/* add on undo list */
		stackList.add(new undoInfo(currentService,teamAScore,teamBScore));
		
		if(team == 0) {
			teamAScore++;
		} else {
			teamBScore++;
		}
		/* verify who is serving */
		int dif=(teamAScore+teamBScore+offsetService) % 8;
		if(dif < 2) {
			currentService = SERVICE_A_UP;
		} else if(dif < 4) {
			currentService = SERVICE_B_DOWN;
		} else if(dif < 6) {
			currentService = SERVICE_A_DOWN;
		} else if(dif < 8) {
			currentService = SERVICE_B_UP;
		}

		/* show current scores */
		showService();
		showScores();
		
		/* play a click sound */
		AudioManager mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
		mAudioManager.playSoundEffect(AudioManager.FX_KEYPRESS_STANDARD,10);
		
		/* is it over? */
		if(hasMatchEnded()) {
			showEnd(teamAScore,teamBScore);
		}
	}
	
	/* show bogo! */
	private void showEnd(int ta,int tb)
	{
		setStatus(String.format("Game Over! Team %s wins!%s",(ta > tb) ? "A" : "B",(ta == 0 || tb == 0) ? " BOGO!" : ""));
		if ((ta == 0 || tb == 0) && (service.getState() == ScoreBoardService.STATE_CONNECTED)) {
			byte [] b = new byte[4];
			
			b[0]=(byte) 0x1b;
			b[1]=(byte) 0x2c;
			b[2]=(byte) 0x3d;
			b[3]=(byte) 0x4c;
			service.write(b);
		}
	}

	/* show current scores */
	private void showScores()
	{
		Ascore.setText(Integer.toString(teamAScore));
		Bscore.setText(Integer.toString(teamBScore));

		/* send to bluetooth */
		if (service.getState() == ScoreBoardService.STATE_CONNECTED) {
			byte [] b = new byte[4];
			
			b[0] = (byte) (0x10 + (teamAScore / 10 % 10));
			b[1] = (byte) (0x20 + (teamAScore % 10));
			b[2] = (byte) (0x30 + (teamBScore / 10 % 10));
			b[3] = (byte) (0x40 + (teamBScore % 10));			
			service.write(b);
		}
	}
	
	/* show current service */
	private void showService()
	{
		switch(currentService) {
		case SERVICE_A_UP:
			Aup.setChecked(true);
			break;
		case SERVICE_B_UP:
			Bup.setChecked(true);
			break;
		case SERVICE_A_DOWN:
			Adn.setChecked(true);
			break;
		case SERVICE_B_DOWN:
			Bdn.setChecked(true);
			break;
		}
		
		/* send to bluetooth */
		if (service.getState() == ScoreBoardService.STATE_CONNECTED) {
			byte [] b = new byte[1];
			
			b[0]= (byte) (0x50+currentService);			
			service.write(b);
		}
	}
	
	private void disableService()
	{
		Aup.setEnabled(false);
		Adn.setEnabled(false);
		Bup.setEnabled(false);
		Bdn.setEnabled(false);
		//mode11.setEnabled(false);
		//mode21.setEnabled(false);
	}
	
	private void enableService()
	{
		Aup.setEnabled(true);
		Adn.setEnabled(true);
		Bup.setEnabled(true);
		Bdn.setEnabled(true);
		//mode11.setEnabled(true);
		//mode21.setEnabled(true);
	}
	
	private void setStatus(String msg)
	{
		statusBar.setText(msg);
	}

	/* undo class */
	public class undoInfo {
		public int service;
		public int Ascore;
		public int Bscore;
		
		public undoInfo(int s,int a,int b) {
			service=s;
			Ascore=a;
			Bscore=b;
		}
	}
	
	// The Handler that gets information back from the BluetoothChatService
	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_STATE_CHANGE:
				switch (msg.arg1) {
				case ScoreBoardService.STATE_CONNECTED:
					setStatus("device connected!!!");
					connectStatus.setChecked(true);
					showScores();
					showService();
					break;
				case ScoreBoardService.STATE_CONNECTING:
					setStatus("trying to connect...");
					connectStatus.setChecked(false);
					break;
				case ScoreBoardService.STATE_LISTEN:
				case ScoreBoardService.STATE_NONE:
					setStatus("device is not connected.");
					connectStatus.setChecked(false);
					break;
				}
				break;
			}
		}
	};
}
